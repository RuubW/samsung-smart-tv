## Samsung Smart TV remote

Based in part on [benreidnet's PHP Samsung TV Remote](https://github.com/benreidnet/samsungtv).

Docker image generated through [PHPDocker.io](https://phpdocker.io/).

Developed and tested with a 2019 Q-series TV.

## Prerequisites

- Your local system has [Docker Desktop](https://www.docker.com/products/docker-desktop), or [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) installed;
- Your TV is on the same network as your local system;
- Your TV is a 2016+ model. Anything older is untested.

## Installation

1. Clone this repository and enter its directory;
2. Copy `.env` to `.env.local` and replace the value of `TV_IP` with your TV's IP address. This value can be found on your TV under `Settings` > `Network` > `Network Status` > `IP Settings`;
3. Create the SSL certificate files:
`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx.key -out nginx.crt`;
4. Run `docker-compose build` to build the environment;
5. Run `docker-compose up -d` to start the environment;
6. Run `docker exec -it samsung-smart-tv-php-fpm bash` to bash into the PHP container;
7. Run `chmod a+rx bin/*` followed by `bin/build dev` to install dependencies and build assets;
8. Run `bin/simple-phpunit` to install the PHPunit files. This is required for the pre-push Git hook to work properly.

## Usage

### Web client

Visit the client at `https://localhost`.

## CLI

The CLI commands below require you to bash into the PHP container:

```bash
docker exec -it samsung-smart-tv-php-fpm bash
```

### Remote command

Run one the following commands to interact with the TV:

```bash
bin/console app:remote --app=3201907018807
bin/console app:remote --key=home
bin/console app:remote --app=3201907018807 --key=home
```

### PHP-cs-fixer

Copy `.php-cs-fixer.dist` over to `.php-cs-fixer` and configure it.

Then run one the following commands to fix code style issues:

```bash
bin/php-cs-fixer fix .
bin/php-cs-fixer fix ./src/Command/RemoteCommand.php # A specific file to fix
```

### PHPStan

Copy `phpstan.neon.dist` over to `phpstan.neon` and configure it.

Then run the following command to perform a static analysis of the codebase:

```bash
bin/phpstan
```

### PHPUnit

Copy `phpunit.xml.dist` over to `phpunit.xml` and configure it.

Then run the following command to perform tests:

```bash
bin/simple-phpunit
```

### Security checker

Run the following command to check for dependency vulnerabilities:

```bash
bin/security-checker security:check
```

## Quirks

- On development environments this application cuts some corners in security by disabling SSL peer verification. Be aware that for this application to run in a production environment, it will need a valid certificate.
- For unclear reasons, occasionally a command will be sent but not executed by the TV.
- Individual TV apps can disable the remote functionality resulting in an "ms.remote.touchDisable" event on all key commands.

## Disclaimer

Executing commands on your TV might result in unexpected results. Use this application at your own risk!
