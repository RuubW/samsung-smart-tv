<?php

declare(strict_types=1);

namespace App\Form;

use stdClass;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RemoteForm.
 */
class RemoteForm
{
    /**
     * @var ?stdClass
     *
     * @Assert\Type(
     *     type="stdClass",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected ?stdClass $app = null;

    /**
     * @var array<string>
     *
     * @Assert\Type(
     *     type="array",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    protected array $keys = [];

    /**
     * Get the selected app.
     *
     * @return ?stdClass
     */
    public function getApp(): ?stdClass
    {
        return $this->app;
    }

    /**
     * Set the selected app.
     *
     * @param stdClass $app
     *
     * @return void
     */
    public function setApp(stdClass $app): void
    {
        $this->app = $app;
    }

    /**
     * Get the selected keys.
     *
     * @return array<string>
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * Set the selected keys.
     *
     * @param array<string> $keys
     *
     * @return void
     */
    public function setKeys(array $keys): void
    {
        $this->keys = $keys;
    }
}
