<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Form\RemoteForm;
use stdClass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RemoteType.
 */
class RemoteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('app', ChoiceType::class, [
                'required'     => false,
                'expanded'     => false,
                'multiple'     => false,
                'label'        => 'remote.form.apps',
                'placeholder'  => 'remote.form.choice',
                'choices'      => $options['app'],
                'choice_value' => 'appId',
                'choice_label' => function (stdClass $app) {
                    return $app->name;
                },
            ])
            ->add('keys', ChoiceType::class, [
                'required'     => false,
                'expanded'     => false,
                'multiple'     => true,
                'label'        => 'remote.form.keys',
                'choices'      => $options['keys'],
                'choice_label' => function (string $key) {
                    return strtoupper($key);
                },
                'help'         => 'remote.form.keys_help',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'remote.form.send',
            ])
            ->add('reset', ResetType::class, [
                'label' => 'remote.form.reset',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RemoteForm::class,
            'keys'       => [],
            'app'        => []
        ]);
    }
}
