<?php

declare(strict_types=1);

namespace App\Command;

use App\Client\RemoteClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RemoteCommand.
 */
class RemoteCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:remote';

    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * @var RemoteClient
     */
    private RemoteClient $remoteClient;

    /**
     * RemoteCommand constructor.
     *
     * @param TranslatorInterface $translator
     * @param RemoteClient        $remoteClient
     * @param ?string             $name
     *
     * @return void
     */
    public function __construct(
        TranslatorInterface $translator,
        RemoteClient $remoteClient,
        ?string $name = null
    ) {
        $this->translator   = $translator;
        $this->remoteClient = $remoteClient;

        parent::__construct($name);
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription(
                $this->translator->trans('remote.command.description')
            )
            ->addOption(
                'app',
                'a',
                InputOption::VALUE_REQUIRED,
                $this->translator->trans('remote.command.argument.app')
            )
            ->addOption(
                'keys',
                'k',
                InputOption::VALUE_REQUIRED,
                $this->translator->trans('remote.command.argument.keys')
            );
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            // Retrieve the app.
            $app = $input->getOption('app');
            // Retrieve the keys.
            $keys = $input->getOption('keys');

            if (!empty($app)) {
                $output->writeln(
                    $this->translator->trans('remote.command.info.app', [
                        'app'  => $app,
                        'host' => $this->remoteClient->getHost(),
                    ])
                );

                // Queue the app launch.
                $this->remoteClient->queueAppLaunch($app);
            }

            if (!empty($keys)) {
                $keys = explode(',', $keys);

                $output->writeln(
                    $this->translator->trans('remote.command.info.keys', [
                        'count' => count($keys),
                        'keys'  => strtoupper(implode(', ', $keys)),
                        'host'  => $this->remoteClient->getHost(),
                    ])
                );

                // Queue the keys.
                $this->remoteClient->queueKeys($keys);
            }

            // Send the queue.
            $this->remoteClient->sendQueue();

            if (!empty($app)) {
                $output->writeln(
                    $this->translator->trans('remote.command.success.app', [
                        'app'  => $app,
                        'host' => $this->remoteClient->getHost(),
                    ])
                );
            }

            if (!empty($keys)) {
                $output->writeln(
                    $this->translator->trans('remote.command.success.keys', [
                        'count' => count($keys),
                        'keys'  => strtoupper(implode(', ', $keys)),
                    ])
                );
            }

            return Command::SUCCESS;
        } catch (\Throwable $exception) {
            $output->writeln(
                $this->translator->trans('remote.command.error', [
                    'message' => $exception->getMessage(),
                ])
            );
        }

        return Command::FAILURE;
    }
}
