<?php

declare(strict_types=1);

namespace App\Client;

use App\Client\Trait\RemoteMessage;

/**
 * Class RemoteQueue.
 */
abstract class RemoteQueue
{
    use RemoteMessage;

    /**
     * @var array<array{message: string, delay: float}>
     */
    protected array $queue = [];

    /**
     * Clear the item queue.
     *
     * @return void
     */
    public function clearQueue(): void
    {
        $this->queue = [];
    }

    /**
     * Add a message to the item queue.
     *
     * @param string $message
     * @param float  $delay
     *
     * @return void
     */
    protected function queueItem(string $message, float $delay = 1.0): void
    {
        $this->queue[] = [
            'message' => $message,
            'delay'   => $delay
        ];
    }

    /**
     * Add a keypress message to the item queue.
     *
     * @param string $key
     * @param float  $delay
     *
     * @return void
     */
    public function queueKey(string $key, float $delay = 1.0): void
    {
        $this->queueItem($this->getKeyPressMessage($key), $delay);
    }

    /**
     * Add keypress message to the item queue.
     *
     * @param array<string> $keys
     * @param float         $delay
     *
     * @return void
     */
    public function queueKeys(array $keys, float $delay = 1.0): void
    {
        foreach ($keys as $key) {
            $this->queueKey($key, $delay);
        }
    }

    /**
     * Add an installed apps message to the item queue.
     *
     * @param float  $delay
     *
     * @return void
     */
    public function queueInstalledApps(float $delay = 1.0): void
    {
        $this->queueItem($this->getInstalledAppsMessage(), $delay);
    }

    /**
     * Add an app launch message to the item queue.
     *
     * @param string $appId
     * @param float  $delay
     *
     * @return void
     */
    public function queueAppLaunch(string $appId, float $delay = 1.0): void
    {
        $this->queueItem($this->getInstalledAppLaunchMessage($appId), $delay);
    }

    /**
     * Get whether there are items present in the queue.
     *
     * @return bool
     */
    protected function hasQueueItems(): bool
    {
        return count($this->queue) > 0;
    }

    /**
     * Get the next item in the queue.
     *
     * @return ?array{message: string, delay: float}
     */
    protected function getNextQueueItem(): ?array
    {
        return array_pop($this->queue);
    }

    /**
     * Send the item queue.
     *
     * @return void
     */
    abstract public function sendQueue(): void;
}
