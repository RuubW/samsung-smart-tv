<?php

declare(strict_types=1);

namespace App\Client\Trait;

/**
 * Trait RemoteMessage.
 */
trait RemoteMessage
{
    /**
     * Get a keypress message.
     *
     * @param string $key
     *
     * @return string
     */
    protected function getKeypressMessage(string $key): string
    {
        $key = strtoupper($key);

        $message = [
            'method' => 'ms.remote.control',
            'params' => [
                'Cmd'          => 'Click',
                'DataOfCmd'    => "KEY_{$key}",
                'Option'       => false,
                'TypeOfRemote' => 'SendRemoteKey',
            ],
        ];

        return json_encode($message, JSON_PRETTY_PRINT) ?: '';
    }

    /**
     * Get an installed apps message.
     *
     * @return string
     */
    protected function getInstalledAppsMessage(): string
    {
        return $this->getChannelEmitMessage('ed.installedApp.get');
    }

    /**
     * Get an installed app icon message.
     *
     * @param string $iconPath
     *
     * @return string
     */
    protected function getInstalledAppIconMessage(string $iconPath): string
    {
        return $this->getChannelEmitMessage('ed.apps.icon', [
            'iconPath' => $iconPath
        ]);
    }

    /**
     * Get an installed app launch message.
     *
     * @param string $appId
     *
     * @return string
     */
    protected function getInstalledAppLaunchMessage(string $appId): string
    {
        return $this->getChannelEmitMessage('ed.apps.launch', [
            'appId'       => $appId,
            'action_type' => 'NATIVE_LAUNCH'
        ]);
    }

    /**
     * Get a channel emit message.
     *
     * @param string $event
     * @param ?array<mixed> $data
     *
     * @return string
     */
    private function getChannelEmitMessage(string $event, ?array $data = []): string
    {
        $message = [
            'method' => 'ms.channel.emit',
            'params' => [
                'event' => $event,
                'to'    => 'host'
            ],
        ];

        if (!empty($data)) {
            $message['params']['data'] = $data;
        }

        return json_encode($message, JSON_PRETTY_PRINT) ?: '';
    }
}
