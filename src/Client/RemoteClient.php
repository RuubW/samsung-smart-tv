<?php

declare(strict_types=1);

namespace App\Client;

use App\Exception\RemoteException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use Ratchet\RFC6455\Messaging\Message;
use Ratchet\RFC6455\Messaging\MessageInterface;
use stdClass;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RemoteClient.
 *
 * Based on https://github.com/benreidnet/samsungtv.
 */
class RemoteClient extends RemoteQueue
{
    /**
     * @var AdapterInterface
     */
    private AdapterInterface $cache;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var RemoteConnector
     */
    private RemoteConnector $remoteConnector;

    /**
     * @var string
     */
    private string $host;

    /**
     * @var string
     */
    private string $protocol;

    /**
     * @var int
     */
    private int $port;

    /**
     * @var string
     */
    private string $appName;

    /**
     * @var bool
     */
    private bool $valid = false;

    /**
     * @var WebSocket
     */
    private WebSocket $websocketConnection;

    // Remote connection URL.
    private const REMOTE_URL = '%s://%s:%d/api/v2/channels/samsung.remote.control?name=%s%s';

    // Token query string part of the remote connection URL.
    private const REMOTE_URL_TOKEN_QUERY = '&token=%s';

    /**
     * RemoteClient constructor.
     *
     * @param AdapterInterface $cache
     * @param LoggerInterface  $logger
     * @param RemoteConnector  $remoteConnector
     * @param string           $host
     * @param string           $protocol
     * @param int              $port
     * @param string           $appName
     *
     * @return void
     */
    public function __construct(
        AdapterInterface $cache,
        LoggerInterface $logger,
        RemoteConnector $remoteConnector,
        string $host,
        string $protocol,
        int $port,
        string $appName
    ) {
        $this->cache           = $cache;
        $this->logger          = $logger;
        $this->remoteConnector = $remoteConnector;
        $this->host            = $host;
        $this->protocol        = $protocol;
        $this->port            = $port;
        $this->appName         = $appName;

        // Connect the client.
        $this->connect();
    }

    /**
     * Connect the client.
     *
     * @return void
     */
    private function connect(): void
    {
        $this->logger->debug('Checking client validity');

        try {
            // The local network can assign a new IP address to the TV between sessions.
            // Invalidate the cache when this occurs.
            $cacheItemHost = $this->cache->getItem('host');
            if ($cacheItemHost->isHit() === false || $cacheItemHost->get() !== $this->host) {
                // Clear the cache.
                $this->cache->clear();

                // Save the new host to the cache.
                $cacheItemHost->set($this->host);
                $this->cache->save($cacheItemHost);
            }

            // Retrieve the cached TV access token.
            $cacheItemToken = $this->cache->getItem('remote_token');
            $tokenQuery = '';
            if ($cacheItemToken->isHit() === true) {
                // Prepare the access token query string.
                $tokenQuery = sprintf(
                    self::REMOTE_URL_TOKEN_QUERY,
                    $cacheItemToken->get()
                );
            }

            // Prepare the remote TV URL.
            $remoteUrl = sprintf(
                self::REMOTE_URL,
                $this->protocol,
                $this->host,
                $this->port,
                base64_encode($this->appName),
                $tokenQuery
            );

            $this->logger->debug("Connecting to {$remoteUrl}");

            $remoteConnector = $this->remoteConnector;
            $remoteConnector($remoteUrl)->then(
                function (WebSocket $websocketConnection) {
                    $this->websocketConnection = $websocketConnection;

                    $this->websocketConnection->on(
                        'message',
                        function (MessageInterface $callbackMessage) {
                            // Handle the callback message.
                            $this->processCallbackMessage($callbackMessage);
                        }
                    );
                },
                function (\Throwable $exception) {
                    $this->logger->error("Could not connect: {$exception->getMessage()}");
                }
            );

            $remoteConnector->getLoop()->run();
        } catch (\Throwable $exception) {
            $this->logger->error("The client could not be connected: {$exception->getMessage()}");
        }
    }

    /**
     * Handle a callback message.
     *
     * @param MessageInterface<Message> $callbackMessage
     *
     * @return void
     *
     * @throws InvalidArgumentException
     * @throws RemoteException
     */
    private function processCallbackMessage(MessageInterface $callbackMessage): void
    {
        $messageJSON    = $callbackMessage->getPayload();
        /** @var stdClass $message */
        $message        = json_decode($messageJSON);
        $messageIsValid = true;

        if (!empty($message->event)) {
            switch ($message->event) {
                case 'ms.channel.connect':
                    // Handle the connect response.

                    // Determine message validity.
                    $messageIsValid = !empty($message->data->clients);

                    if ($messageIsValid === true) {
                        $this->logger->debug('Connected');

                        if (!empty($message->data->token)) {
                            // Save the TV access token to the cache.
                            $cacheItemToken = $this->cache->getItem('remote_token');
                            $cacheItemToken->set($message->data->token);
                            $this->cache->save($cacheItemToken);
                        }

                        // Set client validity.
                        $this->valid = true;

                        // Send the queue items.
                        $this->sendQueueItems();
                    } else {
                        $this->logger->debug('Could not connect');

                        // Delete the TV access token from the cache.
                        $this->cache->deleteItem('remote_token');
                    }

                    break;
                case 'ed.installedApp.get':
                    // Handle the installed apps response.

                    // Determine message validity.
                    $messageIsValid = !empty($message->data->data);

                    if ($messageIsValid === true) {
                        $this->logger->debug('Retrieved installed apps');

                        // Sort the installed apps.
                        usort($message->data->data, function ($a, $b) {
                            return strcasecmp($a->name, $b->name);
                        });

                        // Save the installed apps to the cache.
                        $cacheItemApps = $this->cache->getItem('installed_apps');
                        $cacheItemApps->set($message->data->data);
                        $this->cache->save($cacheItemApps);
                    } else {
                        $this->logger->debug('Could not retrieve installed apps');
                    }

                    break;
                case 'ed.apps.launch':
                    // Handle the app launch response.

                    // Determine message validity.
                    $messageIsValid = $message->data === Response::HTTP_OK;

                    if ($messageIsValid === true) {
                        $this->logger->debug('Launched app');
                    } else {
                        $this->logger->debug('Could not launch app');
                    }

                    break;
            }
        }

        if ($messageIsValid !== true) {
            $this->logger->error("Invalid message: {$messageJSON}");

            throw new RemoteException("Invalid message received: {$messageJSON}");
        }
    }

    /**
     * Return whether the client is valid.
     *
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * Get the host.
     *
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Get a list of installed apps.
     *
     * @param float $delay
     *
     * @return ?array<stdClass>
     */
    public function getInstalledApps(float $delay = 1.0): ?array
    {
        $this->logger->debug('Getting installed apps');

        $cacheItemApps = $this->cache->getItem('installed_apps');
        if ($cacheItemApps->isHit() === true) {
            //            var_dump($cacheItemApps->get()); die();
            return $cacheItemApps->get();
        }

        $this->clearQueue();
        $this->queueInstalledApps($delay);
        $this->sendQueue();

        $cacheItemApps = $this->cache->getItem('installed_apps');
        //var_dump($cacheItemApps->get()); die();
        return $cacheItemApps->get() ?: [];
    }

    /**
     * Launch an app installed.
     *
     * @param string $appId
     * @param float  $delay
     *
     * @return void
     */
    public function launchApp(string $appId, float $delay = 1.0): void
    {
        $this->logger->debug("Launching app {$appId}");

        $this->clearQueue();
        $this->queueAppLaunch($appId, $delay);
        $this->sendQueue();
    }

    /**
     * Send a keypress.
     *
     * @param string $key
     * @param float  $delay
     *
     * @return void
     */
    public function sendKey(string $key, float $delay = 1.0): void
    {
        $this->sendKeys([$key], $delay);
    }

    /**
     * Send a collection of keypresses.
     *
     * @param array<string> $keys
     * @param float         $delay
     *
     * @return void
     */
    public function sendKeys(array $keys, float $delay = 1.0): void
    {
        $implodedKeys = implode(', ', $keys);
        $this->logger->debug("Sending keys {$implodedKeys}");

        $this->clearQueue();
        foreach ($keys as $key) {
            $this->queueKey($key, $delay);
        }
        $this->sendQueue();
    }

    /**
     * Send all items in the item queue.
     *
     * @return void
     */
    private function sendQueueItems(): void
    {
        $queueItem = $this->getNextQueueItem();
        if (!is_null($queueItem)) {
            $this->logger->debug('Sending item...');

            // Prepare and send the message.
            $this->websocketConnection->send($queueItem['message']);

            // Send the next queue key.
            $this->remoteConnector->getLoop()->addTimer($queueItem['delay'], function () {
                $this->sendQueueItems();
            });
        } else {
            $this->logger->debug('No items to send');

            // Close the socket.
            $this->websocketConnection->close();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function sendQueue(): void
    {
        if ($this->valid === false) {
            $this->logger->error('The client is invalid');

            throw new RemoteException('The client is invalid');
        }

        if ($this->hasQueueItems() === false) {
            $this->logger->warning('No keys to send');

            return;
        }

        // Connect the client to send the queue.
        $this->connect();
    }
}
