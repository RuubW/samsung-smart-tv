<?php

declare(strict_types=1);

namespace App\Client;

use Ratchet\Client\Connector as ClientConnector;
use React\EventLoop\Loop;
use React\EventLoop\LoopInterface;
use React\Socket\Connector as SocketConnector;

class RemoteConnector extends ClientConnector
{
    /**
     * @var string
     */
    private string $environment;

    /**
     * @var LoopInterface
     */
    private LoopInterface $loop;

    /**
     * RemoteConnector constructor.
     *
     * @param string $environment
     * @param array  $secureContext
     */
    public function __construct(string $environment, array $secureContext)
    {
        $this->environment = $environment;

        $this->loop      = Loop::get();
        $socketConnector = new SocketConnector(
            (in_array($this->environment, ['dev', 'test'])) ? $secureContext : []
        );

        parent::__construct($this->loop, $socketConnector);
    }

    /**
     * Return the environment.
     *
     * @return string
     */
    public function getEnvironment(): string
    {
        return $this->environment;
    }

    /**
     * Return the event loop.
     *
     * @return LoopInterface
     */
    public function getLoop(): LoopInterface
    {
        return $this->loop;
    }
}
