<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\RemoteForm;
use App\Form\Type\RemoteType;
use App\Client\RemoteClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RemoteController.
 */
class RemoteController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * @var RemoteClient
     */
    private RemoteClient $remoteClient;

    /**
     * @var array<string>
     */
    private array $validKeys;

    /**
     * @var string
     */
    private string $environment;

    /**
     * RemoteController constructor.
     *
     * @param TranslatorInterface $translator
     * @param RemoteClient        $remoteClient
     * @param array<string>       $validKeys
     * @param string              $environment
     *
     * @return void
     */
    public function __construct(
        TranslatorInterface $translator,
        RemoteClient $remoteClient,
        array $validKeys,
        string $environment
    ) {
        $this->translator   = $translator;
        $this->remoteClient = $remoteClient;
        $this->validKeys    = $validKeys;
        $this->environment  = $environment;
    }

    /**
     * Index action.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        // Check the remote client's validity.
        if ($this->remoteClient->isValid() === false) {
            // The remote client is invalid.
            // This usually means that no supported TV was found.

            $this->addFlash(
                'danger',
                $this->translator->trans('remote.controller.no_support')
            );

            // Render the view.
            return $this->render('remote/index.html.twig');
        }

        // Prepare the form.
        $form = $this->createForm(RemoteType::class, new RemoteForm(), [
            'keys' => $this->validKeys,
            'app'  => $this->remoteClient->getInstalledApps()
        ]);

        // Handle the form request.
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var RemoteForm $formData */
                $formData = $form->getData();

                // Retrieve the app.
                $app = $formData->getApp();
                // Retrieve the key(s).
                $keys = $formData->getKeys();

                // Queue the app launch.
                if (!empty($app)) {
                    $this->remoteClient->queueAppLaunch($app->appId);
                }

                // Queue the key(s).
                if (!empty($keys)) {
                    $this->remoteClient->queueKeys($keys);
                }

                // Send the queue.
                $this->remoteClient->sendQueue();

                if (!empty($app)) {
                    $this->addFlash(
                        'success',
                        $this->translator->trans('remote.controller.success.app', [
                            'app' => $app->name,
                        ])
                    );
                }

                if (!empty($keys)) {
                    $this->addFlash(
                        'success',
                        $this->translator->trans('remote.controller.success.keys', [
                            'count' => count($keys),
                            'keys'  => strtoupper(implode(', ', $keys)),
                        ])
                    );
                }
            } catch (\Throwable $exception) {
                // An error occurred while processing the form.

                $this->addFlash(
                    'danger',
                    $this->environment === 'dev' ? $exception->getMessage() : $this->translator->trans('remote.controller.error')
                );
            }
        }

        // Render the view.
        return $this->render('remote/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
