<?php

declare(strict_types=1);

namespace App\Tests;

use App\Client\RemoteConnector;
use App\Exception\RemoteException;
use App\Client\RemoteClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use React\Promise\Internal\FulfilledPromise;
use React\Promise\Internal\RejectedPromise;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\CacheItem;

/**
 * Class RemoteClientTest.
 */
class RemoteClientTest extends TestCase
{
    /**
     * @var MockObject
     */
    private MockObject $cache;

    /**
     * @var MockObject
     */
    private MockObject $logger;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->cache  = $this->createMock(AdapterInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    private function getRemoteClient($valid = true): RemoteClient
    {
        if ($valid === true) {
            $webSocket = $this->createMock(WebSocket::class);
            $promise   = new FulfilledPromise($webSocket);
        } else {
            $promise = new RejectedPromise(new \Exception());
            $promise->catch(function (\Throwable $exception) {});
        }

        $remoteConnector = $this->createMock(RemoteConnector::class);
        $remoteConnector
            ->method('__invoke')
            ->willReturn($promise);

        $remoteClient = new RemoteClient(
            $this->cache,
            $this->logger,
            $remoteConnector,
            '127.0.0.1',
            'wss',
            8002,
            'RemoteClient Test'
        );

        // Set the remote client to valid.
        $this->toggleRemoteClientValidity($remoteClient, $valid);

        return $remoteClient;
    }

    /**
     * Set an object property using reflection.
     * This is useful when the property is not within the public scope.
     *
     * @param object $object
     * @param string $property
     * @param mixed  $value
     *
     * @return void
     */
    private function setObjectPropertyWithReflection(object $object, string $property, mixed $value): void
    {
        try {
            $reflection = new \ReflectionClass(get_class($object));

            $validProperty = $reflection->getProperty($property);
            $validProperty->setAccessible(true);
            $validProperty->setValue($object, $value);
        } catch (\Throwable $exception) {
        }
    }

    /**
     * Manually toggle the remote client validity.
     *
     * @param bool $valid
     *
     * @return void
     */
    protected function toggleRemoteClientValidity(RemoteClient $remoteClient, bool $valid): void
    {
        $this->setObjectPropertyWithReflection($remoteClient, 'valid', $valid);
    }

    /**
     * Get a cache item.
     *
     * @param bool  $isHit
     * @param mixed $value
     *
     * @return CacheItem
     */
    protected function getCacheItem(bool $isHit = false, mixed $value = null): CacheItem
    {
        $cacheItem = new CacheItem();

        if ($isHit === true) {
            $this->setObjectPropertyWithReflection($cacheItem, 'isHit', true);

            $cacheItem->set($value);
        }

        return $cacheItem;
    }

    /**
     * Test whether the connection fails when no valid cache items are set for the host and access token.
     *
     * @return void
     */
    public function testConnectionFailsWhenCacheIsNotSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(2))
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::once())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(2))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient(false);

        // Retrieve the validity.
        $valid = $remoteClient->isValid();

        // Test whether the result was as expected.
        static::assertFalse($valid);
    }

    /**
     * Test whether the connection succeeds when valid cache items are set for the host and remote token.
     *
     * @return void
     */
    public function testConnectionSucceedsWhenCacheIsSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(2))
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::never())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(2))
            ->method('getItem')
            ->will(
                $this->returnValueMap([
                    ['host', $this->getCacheItem(true, 'test')],
                    ['remote_token', $this->getCacheItem(true, 'test')],
                    ['installed_apps', $this->getCacheItem()],
                ])
            );

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Retrieve the validity.
        $valid = $remoteClient->isValid();

        // Test whether the result was as expected.
        static::assertTrue($valid);
    }

    /**
     * Test whether the host is correctly set by the client.
     *
     * @return void
     */
    public function testHostIsSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::once())
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::once())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::once())
            ->method('getItem');

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Retrieve the host.
        $host = $remoteClient->getHost();

        // Test whether the host was set as expected.
        static::assertEquals('127.0.0.1', $host);
    }

    public function testValidIsSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::once())
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::once())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::once())
            ->method('getItem');

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Retrieve the validity.
        $isValid = $remoteClient->isValid();

        // Test whether the result was as expected.
        static::assertTrue($isValid);

        // Set the remote client to invalid.
        $this->toggleRemoteClientValidity($remoteClient, false);

        // Retrieve the host.
        $isValid = $remoteClient->isValid();

        // Test whether the result was as expected.
        static::assertFalse($isValid);
    }

    /**
     * Test whether the sending of the queue fails when the client is invalid.
     *
     * @return void
     */
    public function testSendQueueExecutionFailsWhenClientIsInvalid(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::once())
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::exactly(2))
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::once())
            ->method('getItem');

        // Get the client.
        $remoteClient = $this->getRemoteClient(false);

        // Expect an exception.
        $this->expectException(\Exception::class);
        $this->expectException(RemoteException::class);

        // Send the queue.
        $remoteClient->sendQueue();
    }

    /**
     * Test whether sending of the queue is halted when the queue is empty.
     *
     * @return void
     */
    public function testSendQueueExecutionHaltsWithEmptyQueue(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::once())
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::once())
            ->method('error');

        // Set the expectations for warning logging.
        $this->logger
            ->expects(static::once())
            ->method('warning');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::once())
            ->method('getItem');

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Clear the queue.
        $remoteClient->clearQueue();
        // Send the queue.
        $remoteClient->sendQueue();
    }

    /**
     * Test whether the sending of the queue is executed as expected.
     *
     * @return void
     */
    public function testSendQueueExecutionFailsBecauseHostIsNonExistent(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(4))
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::never())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(4))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Add a key to the queue.
        $remoteClient->queueKey('home');
        // Send the queue.
        $remoteClient->sendQueue();
    }

    /**
     * Test whether the sending of the queue is executed as expected.
     *
     * @return void
     */
    public function testSendQueueExecution(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(4))
            ->method('debug');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(4))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Add a key to the queue.
        $remoteClient->queueKey('home');
        // Send the queue.
        $remoteClient->sendQueue();
    }

    /**
     * Test whether the sending of a single key is executed as expected.
     *
     * @return void
     */
    public function testSendKey(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(5))
            ->method('debug');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(4))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Send a key.
        $remoteClient->sendKey('home');
    }

    /**
     * Test whether the sending of multiple keys is executed as expected.
     *
     * @return void
     */
    public function testSendKeys(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(5))
            ->method('debug');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(4))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Send a list of keys.
        $remoteClient->sendKeys(['home', 'enter']);
    }

    /**
     * Test whether the retrieving of a list of installed apps is executed as expected when its cache item is not set.
     *
     * @return void
     */
    public function testGetInstalledAppsWhenCacheIsNotSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(5))
            ->method('debug');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(6))
            ->method('getItem')
            ->will(
                $this->returnValueMap([
                    ['host', $this->getCacheItem()],
                    ['remote_token', $this->getCacheItem()],
                    ['installed_apps', $this->getCacheItem()],
                ])
            );

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Send a key.
        $remoteClient->getInstalledApps();
    }

    /**
     * Test whether the retrieving of a list of installed apps is executed as expected when its cache item is set.
     *
     * @return void
     */
    public function testGetInstalledAppsWhenCacheIsSet(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(3))
            ->method('debug');

        // Set the expectations for error logging.
        $this->logger
            ->expects(static::never())
            ->method('error');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(3))
            ->method('getItem')
            ->will(
                $this->returnValueMap([
                    ['host', $this->getCacheItem()],
                    ['remote_token', $this->getCacheItem()],
                    ['installed_apps', $this->getCacheItem(true, ['test app data'])],
                ])
            );

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Send a key.
        $remoteClient->getInstalledApps();
    }

    /**
     * Test whether the launching of an app is executed as expected.
     *
     * @return void
     */
    public function testLaunchApp(): void
    {
        // Set the expectations for debug logging.
        $this->logger
            ->expects(static::exactly(5))
            ->method('debug');

        // Set the expectations for cache item retrieval.
        $this->cache
            ->expects(static::exactly(4))
            ->method('getItem')
            ->willReturn($this->getCacheItem());

        // Get the client.
        $remoteClient = $this->getRemoteClient();

        // Send a key.
        $remoteClient->launchApp('3201907018807');    // Netflix
    }
}
